
export default function Header() {
    // alert('test');
    
    if (jQuery && typeof jQuery !== 'undefined' || $ && typeof $ !== 'undefined') {
    	
            $(window).scroll(function() {    
                var scroll = $(window).scrollTop();
                    if (scroll >= 50) {
                        $("header").addClass("scrollheader");
                        $('header .logo-icon-only').fadeIn();
                        $('header .logo').fadeOut();
                        $("header").addClass("fixed-layout")
                    } else {
                        $("header").removeClass("scrollheader");
                        $('header .logo-icon-only').fadeOut();
                        $('header .logo').fadeIn();
                        $("header").removeClass("fixed-layout")
                    }
                });
            // menu active 
            $('.navbar-nav.menus .nav-link').click(function() {
                $(this).addClass("active");
                $(".navbar-nav.menus .nav-link").not(this).removeClass("active");    
            });
            $('.close-button').click( function(){
                //alert()
                $(this).parent().fadeOut();
            });
            $('#button-basic').click( function(){
                //alert()
                $('#dropdown-basic').fadeToggle();
            });
   		}
    }

