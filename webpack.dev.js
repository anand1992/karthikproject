const HtmlWebpackPlugin = require('html-webpack-plugin');
const common = require('./webpack.common');
const merge = require('webpack-merge');
const path = require('path');
const fs = require('fs');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');



// Our function that generates our html plugins
function generateHtmlPlugins(templateDir) {
  // Read files in template directory
  const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir))
  return templateFiles.map(item => {
    // Split names and extension
    const parts = item.split('.')
    const name = parts[0]
    const extension = parts[1]
    // Create new HTMLWebpackPlugin with options
    return new HtmlWebpackPlugin({
      filename: `${name}.html`,
      template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`)
    })
  })
}

// Call our function on our views directory.
const htmlPlugins = generateHtmlPlugins('./src/templates/html')
const port = 4300;

module.exports = merge(common, {
  devServer: {
    // publicPath: path.join(__dirname, 'dist'),
    contentBase: path.join(__dirname),
    writeToDisk: true,
    port: port,
    open: true,
    hotOnly: true,
    hot: true,
    inline: true,
    progress: true,
    watchContentBase: true,
    watchOptions: {
      ignored: /node_modules/
    },
    onListening: function (server) {
      console.log("\x1b[36m%s\x1b[0m", '--------------------------------------------------------------------------------------------');
      console.log("\x1b[36m%s\x1b[0m", `Webpack Serving Static file directory: ${server.options.contentBase}`);
      console.log("\x1b[36m%s\x1b[0m", `Webpack Dev server is running at port : ${server.options.port}`);
      console.log("\x1b[36m%s\x1b[0m", '--------------------------------------------------------------------------------------------');
    },
  },
  devtool: 'source-map',
  mode: 'development',
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    // publicPath: path.resolve(__dirname, 'dist')
  },
  plugins: [
    new CleanWebpackPlugin({
      // dry: true,
      cleanStaleWebpackAssets: false,
      protectWebpackAssets: false,
    }),
  ].concat(htmlPlugins),
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          'style-loader', //3. Inject styles into DOM
          {
            loader: 'css-loader',
            options: {
              url: false, sourceMap: true
            }
          }, //2. Turns css into commonjs
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ] //1. Turns sass into css
      }
    ]
  }
});
